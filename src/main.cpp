#include "amxhandler.h"
#include <amxrt/amxrt.h>

#include "utils/vector.h"
#include "utils/sharedptr.h"
class AmxrtResource {
public:
    AmxrtResource() {
        amxrt_new();
    }

    ~AmxrtResource() {
        amxrt_stop();
        amxrt_delete();
    }
};

int main(int argc, char* argv[]) {
    AmxrtResource amxrtResource; // RAII for amxrt resources
    int retval = 0;
    int index = 0;

    lxcd::SharedPtr<amxc_var_t> config(amxrt_get_config());
    lxcd::SharedPtr<amxd_dm_t> dm(amxrt_get_dm());
    lxcd::SharedPtr<amxo_parser_t> parser(amxrt_get_parser());
    retval = amxrt_config_init(argc, argv, &index, NULL);
    if (retval != 0) {
        printf("Configuration initialization failed\n");
        return retval;
    }

    retval = amxrt_load_odl_files(argc, argv, index);
    if (retval != 0) {
        printf("Loading ODL files failed\n");
        return retval;
    }

    // Add entry point to the parser
    amxo_parser_add_entry_point(parser.get(), amxrt_dm_save_load_main);

    // Connect
    retval = amxrt_connect();
    if (retval != 0) {
        printf("Connection failed\n");
        return retval;
    }

    // Enable system signals if available
    auto syssigs = GET_ARG(config.get(), "system-signals");
    if (syssigs) {
        amxrt_enable_syssigs(syssigs);
    }

    // Create event loop
    retval = amxrt_el_create();
    if (retval != 0) {
        printf("Event loop creation failed\n");
        return retval;
    }

    // Register or wait
    retval = amxrt_register_or_wait();
    if (retval != 0) {
        printf("Register or wait failed\n");
        return retval;
    }
    amxrt_el_start();

    return retval;
}
